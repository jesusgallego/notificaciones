package es.ua.eps.notificaciones;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

public class DialogsActivity extends AppCompatActivity implements MiDialog.DialogCallbackInterface {

    TextView textView;
    String[] colorItems = {"Blanco y Negro", "Negro y Blanco", "Negro y Verde"};
    String[] sizeItems = {"Pequeño", "Mediano", "Grande"};

    static final String COLOR_TAG = "COLOR_TAG";
    static final String SIZE_TAG = "SIZE_TAG";

    int colorSelected = 0;
    int sizeSelected = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogs);

        textView = (TextView) findViewById(R.id.textView);
    }

    public void onColorClicked (View v) {

        Bundle bundle = new Bundle();
        bundle.putString("Title", "Color");
        bundle.putInt("Selected", colorSelected);
        bundle.putStringArray("Items", colorItems);

        MiDialog dialog = new MiDialog();
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), COLOR_TAG);
    }

    public void onSizeClicked (View v) {
        Bundle bundle = new Bundle();
        bundle.putString("Title", "Tamaño");
        bundle.putInt("Selected", sizeSelected);
        bundle.putStringArray("Items", sizeItems);

        MiDialog dialog = new MiDialog();
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), SIZE_TAG);
    }

    public void back(View v) {
        finish();
    }

    private void changeColor(String color) {
        int bg = 0, text = 0;
        switch (color) {
            case "Blanco y Negro":
                bg = android.R.color.white;
                text = android.R.color.black;
                break;
            case "Negro y Blanco":
                bg = android.R.color.black;
                text = android.R.color.white;
                break;
            case "Negro y Verde":
                bg = android.R.color.black;
                text = android.R.color.holo_green_light;
                break;
        }

        textView.setBackgroundResource(bg);
        textView.setTextColor(ContextCompat.getColor(this, text));
    }

    private void changeSize(String sizeType) {
        float size = 0;
        switch (sizeType) {
            case "Pequeño":
                size = 8;
                break;
            case "Mediano":
                size = 12;
                break;
            case "Grande":
                size = 20;
                break;
        }

        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    @Override
    public void acceptDialog(String tag, int which) {
        switch (tag) {
            case COLOR_TAG:
                colorSelected = which;

                changeColor(colorItems[which]);
                break;
            case SIZE_TAG:
                sizeSelected = which;

                changeSize(sizeItems[which]);
                break;
        }

    }

    @Override
    public void cancelDialog(String tag, int which) {
        switch (tag) {
            case COLOR_TAG:
                changeColor(colorItems[which]);
                break;
            case SIZE_TAG:
                changeSize(sizeItems[which]);
                break;
        }
    }
}
