package es.ua.eps.notificaciones;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import java.util.List;

/**
 * Created by mastermoviles on 18/10/16.
 */
public class MiDialog extends DialogFragment {

    DialogCallbackInterface callback;

    String title;
    String[] items;
    int selected;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();

        this.title = args.getString("Title");
        this.selected = args.getInt("Selected");
        this.items = args.getStringArray("Items");

        return new AlertDialog.Builder(getActivity())
                .setTitle(this.title)
                .setSingleChoiceItems(items, selected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.acceptDialog(getTag(), which);
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.cancelDialog(getTag(), selected);
                    }
                })
                .create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            callback = (DialogCallbackInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(String.format("%s %s DialogCallbackInterface",context.toString(), R.string.must_implement));
        }
    }

    public interface DialogCallbackInterface {
        void acceptDialog(String tag, int which);
        void cancelDialog(String tag, int which);
    }
}
