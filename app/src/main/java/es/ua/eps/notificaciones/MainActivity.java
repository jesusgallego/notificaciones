package es.ua.eps.notificaciones;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText text;
    List<String> tasks;
    TextView taskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tasks = new ArrayList<String>();

        text = (EditText)findViewById(R.id.text);
        taskList = (TextView) findViewById(R.id.task_list);
    }

    public void newToast(View v) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.toast_layout, null);

        TextView textMsg = (TextView)layout.findViewById(R.id.textMsg);

        String msg = "Write a text first";
        if (isValidText()) {
            msg = text.getText().toString();
        }
        textMsg.setText(msg);

        Toast toast = new Toast(this);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(layout);
        toast.show();

        text.setText("");
    }

    public void newTask(View v) {
        String msg = "Write a text first";

        if (isValidText()) {
            msg = text.getText().toString();
            tasks.add(msg);

            Snackbar.make(v, "Task added Successfully!", Snackbar.LENGTH_LONG)
                    .setAction("Undo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tasks.remove(tasks.size() - 1);
                            taskList.setText(listToString());
                        }
                    })
                    .show();

            taskList.setText(listToString());

            text.setText("");
        } else {
            Snackbar.make(v, msg, Snackbar.LENGTH_SHORT).show();
        }

    }

    public void goToDialogs (View v) {
        Intent intent = new Intent(this, DialogsActivity.class);
        startActivity(intent);
    }

    public void goToNotifications (View v) {
        Intent intent = new Intent(this, NotificationsActivity.class);
        startActivity(intent);
    }

    private boolean isValidText() {
        return !text.getText().toString().equals("");
    }

    private String listToString () {
        String list = "";
        for (String s : tasks) {
            list += s + "\n";
        }
        return list;
    }
}
